// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require bootstrap-table
//= require locale/bootstrap-table-fr-FR
//= require extensions/bootstrap-table-export
//= require toastr
//= require bootstrap-switch
//= require_tree .

$.fn.bootstrapSwitch.defaults.onText = 'Oui';
$.fn.bootstrapSwitch.defaults.offText = 'Non';
$.fn.bootstrapSwitch.defaults.onColor = 'primary';

function saisieNote(classe, cycle, test){
  $('#fieldClassroomId').val(classe);
  $('#fieldCycleId').val(cycle);
  $('#fieldTestId').val(test);
  $('#modaleAjoutNote').modal('show');
}

function valideNote(note){
  $('#fieldNoteId').val(note);
}

function selectTest(cycle_id) {
  var test_id = $('#test_id').val();
  var url = "/global/" + cycle_id + "?test_id=" + test_id;
  window.open(url,"_self");
}

function sendActivation(user_id) {
  jQuery.ajax({  type: "POST",
                 url: "/users/" + user_id + "/send_activation" ,
                 dataType: "json",
                 data: {id: user_id},
                 success: function (data) {
                  toastr.success("Le courriel d'activation a été envoyé");
                 },
                 error : function(resultat, statut, erreur){
                  toastr.success("Le courriel d'activation a été envoyé");
                }
  });
}

function buttonActive(active) {
  button = document.getElementById("buttonValidateInscription")
  button.disabled = !active;
  if (active) { button.title = "" }
  else { button.title = "Vous devez compléter tout le formulaire" }
}

function validateInscription() {
  checked = $(".checkbox_cycle:checked").length;
  checked2 = $(".checkbox_niveau:checked").length;

  if(checked && checked2) {
    buttonActive(true);
  } else {
    buttonActive(false);
  }
};

$(document).ready(function(){
  $('#table').bootstrapTable();
  $('#table_docs').bootstrapTable();
  $('.boutonAfficheModaleAjoutEnseignant').click(function() {
    classroom_id = $(this).val();
    $('#fieldClassroomId').val(classroom_id);
    $('#modaleAjoutEnseignant').modal('show');
  });
  $('#boutonAjoutEnseignant').click(function() {
    $('#modaleAjoutEnseignant').modal('hide');
  });
  $(document).on('input', '#otherClassroom', function() {$('#otherCheck').prop('checked', true);});
  $('.collapse').collapse();
  $('.my-checkbox').bootstrapSwitch();
  $('.my-checkbox').on('switchChange.bootstrapSwitch', function(event, state) {
    this.value = state;
  });
});
