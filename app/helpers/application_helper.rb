module ApplicationHelper
  def classe_page(url)
    inclu = false
    ['/sign_in', '/user_sessions', '/password_resets'].each do |path|
      inclu ||= url.include?(path)
    end
    return inclu ? "signup-container" : "page-container"
  end

  def markdown(text)
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML,
                                       no_intra_emphasis: true,
                                       fenced_code_blocks: true,
                                       disable_indented_code_blocks: true,
                                       tables: true,
                                       underline: true,
                                       highlight: true
                                      )
    return markdown.render(text).html_safe
  end
end
