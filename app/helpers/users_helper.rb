module UsersHelper

  def delete user
    if user.results.count>0
      user.active = false
      user.save
      return false
    else
      user.classrooms.each do |classroom|
        unless classroom.users.count>1
          classroom.destroy
        end
      end
      user.destroy
      return true
    end
  end

end
