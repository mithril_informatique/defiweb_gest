module ClassroomsHelper

  def delete classroom
    if classroom.results.count > 0
      message = 'La classe a déjà des résultats, elle ne peut pas être supprimée.'
    else
      if classroom.destroy
        message = "La classe a été supprimé."
      else
        message = "Erreur lors de la suppression de la classe"
      end
    end
  end
  
end
