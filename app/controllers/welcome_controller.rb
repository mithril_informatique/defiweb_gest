class WelcomeController < ApplicationController

  def index
    @user = current_user
    if !@user
      redirect_to sign_in_path
    elsif @user.bloque
      message = "Votre compte utilisateur est bloqué. Veuillez contacter un administrateur"
      current_user_session.destroy
      redirect_to root_path, notice: message
    end
  @tests = Test.all
  end

  def global
    @cycle = params[:id].to_i>0 ? Cycle.find(params[:id]) : nil
    @titre = "Tous les résultats"
    @titre += " du #{@cycle.name}" if @cycle
    @vide = params["vide"]=="1"
    classrooms = Classroom.joins(:circonscription).order("circonscriptions.name", "name")
    @classrooms = []
    if @cycle
      classrooms.each do |classroom|
        @classrooms << classroom
      end
    else
      @classrooms = classrooms.joins(:circonscription).order("circonscriptions.name", "name")
    end
    @cycles = Cycle.order(:name)
    @tests = Test.order(:order)
    @test = params["test_id"].to_i>0 ? Test.find(params["test_id"].to_i) : nil
  end

end
