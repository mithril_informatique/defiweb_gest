class UsersController < ApplicationController

  include UsersHelper

  before_action :only_admins_or_pilotes, only: [:index]
  before_action :only_admins, except: [:index, :new, :create, :create_with_existing_classroom, :active, :reset_password, :send_activation, :account, :destroy, :update]
  before_action :only_logged_users, except: [:new, :create, :active, :reset_password]
  before_action :set_user, only: [:show, :destroy, :edit, :update, :active, :reset_password, :send_activation]
  before_action :only_owner, only: [:destroy]
  before_action :set_lists, only: [:new, :create, :destroy, :edit, :update]

  def index
    @users = nil
    if params["type"]=="admins"
      @users = User.order(:created_at).where(admin: true)
    elsif params["type"]=="pilotes"
      @users = User.order(:created_at).where(pilote: true)
    else
      @users = User.order(:created_at).where(admin: [false, nil], pilote: [false, nil])
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  def new
    @user = User.new
    @consigne = Instruction.where(name:"inscription").first
  end

  def create
    @user = User.new(user_params)
    @consigne = Instruction.where(name:"inscription").first
    @user.active = false # temporaire en attendant d'avoir la confirmation par email
    if @user.save
      message = "L'utilisateur #{@user.name} a été créé."
      unless @user.active
        message += "Un email vous a été envoyé sur votre boîte académique pour valider votre inscription. Le lien sera valide pendant 24h."
        PasswordResetMailer.activate_account(@user).deliver_now
      end
      email_etablissement = params["email_etablissement"]
      if classroom = Classroom.create(name: params["class_name"],
                                      circonscription_id: params["circonscription_id"],
                                      ecole: params["etablissement"],
                                      school_type_id: params["school_type_id"],
                                      email_etablissement: email_etablissement,
                                      other_level: params["other_classroom"])
        @cycles.each do |cycle|
         if params[cycle.name]
           classroom.cycles << cycle unless classroom.cycles.include?(cycle)
         end
        end
        @levels.each do |level|
         if params[level.name]
           classroom.levels << level unless classroom.levels.include?(level)
         end
        end
        @user.classrooms << classroom
        redirect_to root_path, notice: message
      else
        @user.delete
        render :action => :new
      end
    else
      render :action => :new
    end
  end

  def create_with_existing_classroom
    new = false
    @consigne = Instruction.where(name:"inscription").first
    classroom = Classroom.find(params["classroom_id"])
    if classroom
      email = params["email"]
      @user = User.where(email: email).first
      unless @user
        @user = User.new
        @user.first_name = params["first_name"]
        @user.last_name = params["last_name"]
        @user.email = email
        @user.active = false # temporaire en attendant d'avoir la confirmation par email
        password = SecureRandom.base64(6)
        @user.password = password
        @user.password_confirmation = password
        new = true
      end
      @user.classrooms << classroom
      if @user.save
        PasswordResetMailer.reset_email(@user, "#{current_user.name} vous a inscrit à une classe pour le Défiweb.\nPour y participer vous devez cliquer sur ce lien et vous créer un mot de passe.").deliver_now if new
        redirect_to root_path, notice: "Nouvel enseignant ajouté"
      else
        redirect_to root_path, notice: "Impossible d'ajouter le nouvel enseignant"
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if delete @user
      respond_to do |format|
        format.html { redirect_to users_url, notice: "L'utilisateur #{@user.name} a été supprimé" }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to users_url, notice: "L'utilisateur #{@user.name} a déjà saisi des résultats, il a donc été désactivé" }
        format.json { head :no_content }
      end
    end
  end

  # GET /users/1/edit
  def edit
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      parametres = user_params
      if @user.update(parametres)
        format.html {
          if (current_user.admin or current_user.pilote)
            redirect_to users_path, notice: "L'utilisateur #{@user.name} vient d'être mis à jour"
          else
            redirect_to root_path, notice: "L'utilisateur #{@user.name} vient d'être mis à jour"
          end
        }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /users/1/active
  def active
    if @user.perishable_token==params["token"]
      @user.active = true
      @user.save
      redirect_to sign_in_path, notice: "Votre compte a été activé"
    else
      redirect_to sign_in_path, notice: "Votre lien n'est plus valide"
    end
  end

  # POST /users/1/send_activation
  def send_activation
    PasswordResetMailer.activate_account(@user).deliver_now unless @user.active
  end

  # GET /account
  def account
    @user = current_user
  end

  # GET /users/emails
  def emails
    @cycles = {}
    users = User.where(active: true, bloque: [false, nil]).order(:last_name, :first_name)
    Cycle.all.each do |cycle|
      text = ""
      users.each do |user|
        user.classrooms.each do |classroom|
          if classroom.cycle_ids.include?(cycle.id)
            add = "#{user.email} #{user.last_name} #{user.first_name}\n"
            text << add unless text.include?(add)
          end
        end
      end
      @cycles[cycle.name] = text
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def set_lists
      @circonscriptions = Circonscription.order(:name)
      @cycles = Cycle.order(:name)
      @levels = Level.order(:order)
      @school_types = SchoolType.order(:name)
    end

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name, :active, :admin, :pilote, :bloque)
    end
end
