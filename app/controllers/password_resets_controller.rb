class PasswordResetsController < ApplicationController
  def new
    @consigne = Instruction.where(name:"oubli").first
  end

  def create
    @user = User.find_by_email(params[:email])
    if @user
      @user.deliver_password_reset_instructions!
      redirect_to root_path, notice: "Des instructions pour réinitialiser votre mot de passe vous ont été envoyé par courriel."
    else
      redirect_to "/password_resets/new", notice: "Aucun utilisateur avec cette adresse n'est inscrit."
    end
  end

  def edit
    @user = User.find_by(perishable_token: params[:id])
    redirect_to sign_in_path, notice: "Votre lien a expiré. Merci de recommencer la procédure." unless @user
  end

  def update
    @user = User.find_by(perishable_token: params[:id])
    @user.active = true
    if @user.update(password: params["password"], password_confirmation: params["password_confirmation"])
      redirect_to root_path, notice: "Le mot de passe a bien été modifié"
    else
      render :edit
    end
  end

  private

  def password_reset_params
    params.require(:user).permit(:password, :password_confirmation)
  end
end
