class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user_session, :current_user, :only_logged_users, :only_admins, :only_classroom_owner

  before_action :get_config

  private
    def current_user_session
      return @current_user_session if defined?(@current_user_session)
      @current_user_session = UserSession.find
    end

    def current_user
      return @current_user if defined?(@current_user)
      @current_user = current_user_session && current_user_session.user
    end

    def only_logged_users
      unless current_user
        redirect_to sign_in_path, notice: "Vous devez être connecté pour pouvoir accéder à cette page"
      end
    end

    def only_admins
      unless (current_user && current_user.admin)
        redirect_to :back, notice: "Vous devez être administrateur pour pouvoir accéder à cette page"
      end
    end

    def only_admins_or_pilotes
      unless (current_user && (current_user.admin or current_user.pilote))
        redirect_to :back, notice: "Vous devez être administrateur ou pilote pour pouvoir accéder à cette page"
      end
    end

    def only_classroom_owner
      unless (current_user && current_user.admin)
        unless (@classroom.users.include?(current_user))
          redirect_to :back, notice: "Vous n'avez pas les droits pour accéder à cette page"
        end
      end
    end

    def only_classroom_cycle
      unless (@classroom.cycles.include?(@cycle))
        redirect_to root_path, notice: "La classe sélectionnée n'a pas le cycle demandé"
      end
    end

    def only_owner
      redirect_to root_path, notice: "Vous n'avez pas les droits pour accéder à cette page" unless (current_user && (current_user.admin or current_user.id==@user.id))
    end

  protected

  def handle_unverified_request
    # raise an exception
    fail ActionController::InvalidAuthenticityToken
    # or destroy session, redirect
    if current_user_session
      current_user_session.destroy
    end
    redirect_to root_url
  end

  def get_config
    @config = Config.first
  end

end
