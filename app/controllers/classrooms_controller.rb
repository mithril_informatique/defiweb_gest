class ClassroomsController < ApplicationController
  before_action :only_admins_or_pilotes, only: [:index, :complet]
  before_action :only_admins, except: [:new, :create, :update, :edit, :index, :complet, :del_user]
  before_action :set_classroom, only: [:show, :edit, :update, :destroy, :del_user]
  before_action :only_classroom_owner, only: [:show, :edit, :update, :del_user]
  before_action :set_lists, only: [:new, :create, :destroy, :edit, :update]

  include ClassroomsHelper

  # GET /classrooms
  # GET /classrooms.json
  def index
    @classrooms = Classroom.all
  end

  # GET /classrooms/complet
  # GET /classrooms/complet.json
  def complet
    @classrooms = Classroom.all
  end

  # GET /classrooms/1
  # GET /classrooms/1.json
  def show
  end

  # GET /classrooms/new
  def new
    @classroom = Classroom.new
  end

  # GET /classrooms/1/edit
  def edit
  end

  # POST /classrooms
  # POST /classrooms.json
  def create
    @classroom = Classroom.new(classroom_params)
    @classroom.users << current_user
    @classroom.other_level = params["other_classroom"]
    respond_to do |format|
      if @classroom.save
        @cycles.each do |cycle|
          if params[cycle.name]
            @classroom.cycles << cycle unless @classroom.cycles.include?(cycle)
          end
        end
        @levels.each do |level|
          if params[level.name]
            @classroom.levels << level unless @classroom.levels.include?(level)
          end
        end
        format.html { redirect_to root_path, notice: 'Classroom was successfully created.' }
        format.json { render :show, status: :created, location: @classroom }
      else
        format.html { render :new }
        format.json { render json: @classroom.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /classrooms/1
  # PATCH/PUT /classrooms/1.json
  def update
    parametres = classroom_params
    respond_to do |format|
      if @classroom.update(parametres)
        @cycles.each do |cycle|
          if params[cycle.name]
            @classroom.cycles << cycle unless @classroom.cycles.include?(cycle)
          else
            @classroom.cycles.delete(cycle)
          end
        end
        @levels.each do |level|
          if params[level.name]
            @classroom.levels << level unless @classroom.levels.include?(level)
            if level.name=="autre"
              @classroom.other_level = params["other_classroom"]
              @classroom.save
            end
          else
            @classroom.levels.delete(level)
          end
        end
        format.html { redirect_to root_path, notice: 'La classe a été modifiée' }
        format.json { render :show, status: :ok, location: @classroom }
      else
        format.html { render :edit }
        format.json { render json: @classroom.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy
    message = delete @classroom
    redirect_to :back, notice: message
  end

  # DELETE /classrooms/1/users/2
  def del_user
    message = ""
    user = User.find(params[:user_id])
    if @classroom.users.count > 1
      @classroom.users = @classroom.users - [user]
      if @classroom.save
        message = "La classe a été retirée de l'utilisateur #{user.name}"
      end
    else
      if @classroom.users.include?(user)
        message = delete @classroom
      end
    end
    redirect_to :back, notice: message
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_classroom
      @classroom = Classroom.find(params[:id])
    end

    def set_lists
      @circonscriptions = Circonscription.order(:name)
      @cycles = Cycle.order(:name)
      @levels = Level.order(:order)
      @school_types = SchoolType.order(:name)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def classroom_params
      params.require(:classroom).permit(:name, :circonscription_id, :ecole, :school_type_id, :email_etablissement)
    end
end
