class ResultsController < ApplicationController
  before_action :only_admins, except: [:create, :saisie, :index]
  before_action :only_admins_or_pilotes, only: [:index]
  before_action :set_classroom, only: [:create, :update, :saisie]
  before_action :only_classroom_owner, only: [:saisie, :create, :update]
  before_action :only_classroom_cycle, only: [:saisie, :create, :update]
  before_action :set_result, only: [:show, :edit, :update, :destroy]

  # GET /results
  # GET /results.json
  def index
    @results = Result.all
  end

  # GET /results/1
  # GET /results/1.json
  def show
  end

  # GET /results/new
  def new
    @result = Result.new
  end

  # GET /results/1/edit
  def edit
  end

  # POST /results
  # POST /results.json
  def create
    @result = Result.where(classroom_id: params["classroom_id"], cycle_id: params["cycle_id"], test_id: params["test_id"]).first
    if params["note_id"].to_i==0
      @result.destroy if @result
      redirect_to :back, notice: 'Résultat remis à zéro'
    else
      if @result
        @result.note_id = params["note_id"]
      else
        @result = Result.new(result_params)
      end
      @result.user_id = current_user.id
      respond_to do |format|
        if @result.save
          format.html { redirect_to :back, notice: 'Résultat enregistré' }
          format.json { render :show, status: :created, location: @result }
        else
          format.html { redirect_to :back, notice: 'Erreur lors de l\'enregistrement' }
          format.json { render json: @result.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /results/1
  # PATCH/PUT /results/1.json
  def update
    respond_to do |format|
      if @result.update(result_params)
        format.html { redirect_to :back, notice: 'Result was successfully updated.' }
        format.json { render :show, status: :ok, location: @result }
      else
        format.html { render :edit }
        format.json { render json: @result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /results/1
  # DELETE /results/1.json
  def destroy
    @result.destroy
    respond_to do |format|
      format.html { redirect_to results_url, notice: 'Result was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /results/saisie
  def saisie
    @tests = Test.order(:order)
    @notes = Note.order(:id)
    @results = Result.where(classroom_id: @classroom.id,
                            cycle_id: @cycle)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_result
      @result = Result.find(params[:id])
    end

    def set_classroom
      @classroom = Classroom.find(params["classroom_id"])
      @cycle = Cycle.find(params["cycle_id"])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def result_params
      params.permit(:classroom_id, :test_id, :cycle_id, :note_id)
    end
end
