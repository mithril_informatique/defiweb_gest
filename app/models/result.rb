class Result < ActiveRecord::Base
  validates :classroom_id, :test_id, :cycle_id, :note_id, :user_id, :presence => true
  belongs_to :classroom
  belongs_to :test
  belongs_to :cycle
  belongs_to :note
  belongs_to :user
end
