class Test < ActiveRecord::Base
  has_many :results
  has_attached_file :picto, styles: { medium: "300x300>", thumb: "100x100>", xs: "50x50" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :picto, content_type: /\Aimage\/.*\z/
end
