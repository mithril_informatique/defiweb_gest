class User < ActiveRecord::Base
  validates :email, :first_name, :last_name, :presence => true

  validate :verify_email

  has_and_belongs_to_many :classrooms
  has_many :results

  acts_as_authentic do |c|
    c.log_in_after_create = false
  end

  def deliver_password_reset_instructions!
    reset_perishable_token!
    PasswordResetMailer.reset_email(self).deliver_now
  end

  def name
    return "#{self.first_name} #{self.last_name.upcase}"
  end

  private
    def verify_email
      address = self.email.downcase
      match = Academie.pluck(:name).detect { |name| address.end_with?(name) }
      errors.add(:email, "Votre courriel ne correspond pas à une adresse académique") unless match
    end
end
