class Classroom < ActiveRecord::Base
  belongs_to :circonscription
  belongs_to :school_type
  has_and_belongs_to_many :users
  has_and_belongs_to_many :levels
  has_and_belongs_to_many :cycles
  has_many :results

  def cycle?(cycle_id)
    return self.cycle_ids.include?(cycle_id)
  end

  def level?(level_id)
    return self.level_ids.include?(level_id)
  end

end
