class Document < ActiveRecord::Base
  belongs_to :cycle
  belongs_to :test
  validates :cycle, :test, :presence => true
  has_attached_file :file
  validates_attachment_presence :file
  validates_attachment_file_name :file, matches: [/pdf\z/, /zip\z/]
end
