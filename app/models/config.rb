class Config < ActiveRecord::Base
  has_attached_file :image_fond, styles: { medium: "1200x1200", thumb: "100x100" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image_fond, content_type: /\Aimage\/.*\z/
  has_attached_file :bandeau, styles: { medium: "480x150", thumb: "100x100" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :bandeau, content_type: /\Aimage\/.*\z/
end
