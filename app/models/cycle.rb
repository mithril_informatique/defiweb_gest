class Cycle < ActiveRecord::Base
  has_and_belongs_to_many :classrooms
  has_many :results
end
