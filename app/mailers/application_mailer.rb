class ApplicationMailer < ActionMailer::Base
  default from: "defiweb@ac-reunion.fr"
  layout 'mailer'
end
