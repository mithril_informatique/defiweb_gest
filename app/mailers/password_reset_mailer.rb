class PasswordResetMailer < ApplicationMailer
  def reset_email(user, message=nil)
    @message = message
    @user = user
    mail(to: @user.email, subject: 'Défiweb - Réinitialisation de mot de passe')
  end

  def activate_account(user)
    @user = user
    mail(to: @user.email, subject: 'Défiweb - Activez votre compte')
  end
end
