json.extract! level, :id, :name, :order, :created_at, :updated_at
json.url level_url(level, format: :json)
