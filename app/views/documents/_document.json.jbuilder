json.extract! document, :id, :name, :description, :file, :cycle_id, :test_id, :created_at, :updated_at
json.url document_url(document, format: :json)
