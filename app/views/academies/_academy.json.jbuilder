json.extract! academy, :id, :name, :description, :created_at, :updated_at
json.url academy_url(academy, format: :json)
