json.extract! config, :id, :image_fond, :bandeau, :inscription_automatique, :created_at, :updated_at
json.url config_url(config, format: :json)
