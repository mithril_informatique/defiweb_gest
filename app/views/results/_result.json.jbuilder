json.extract! result, :id, :classroom_id, :test_id, :cycle_id, :note_id, :user_id, :created_at, :updated_at
json.url result_url(result, format: :json)
