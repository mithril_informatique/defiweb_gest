json.extract! note, :id, :name, :value, :created_at, :updated_at
json.url note_url(note, format: :json)
