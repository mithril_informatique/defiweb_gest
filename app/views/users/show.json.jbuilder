json.extract! @user, :id, :name, :email, :school, :active, :last_login_at, :current_login_at, :current_login_ip, :last_login_ip, :login_count, :failed_login_count, :last_request_at, :created_at, :updated_at
json.circonscription @user.circonscription.name
json.cycle @user.cycle.name
