json.extract! classroom, :id, :name, :circonscription_id, :cycle, :ecole, :school_type, :created_at, :updated_at
json.url classroom_url(classroom, format: :json)
