json.extract! instruction, :id, :name, :description, :created_at, :updated_at
json.url instruction_url(instruction, format: :json)
