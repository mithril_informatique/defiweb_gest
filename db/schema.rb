# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180930174556) do

  create_table "academies", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "circonscriptions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "classrooms", force: :cascade do |t|
    t.string   "name"
    t.integer  "circonscription_id"
    t.string   "ecole"
    t.integer  "school_type_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "email_etablissement"
    t.string   "other_level"
  end

  add_index "classrooms", ["circonscription_id"], name: "index_classrooms_on_circonscription_id"
  add_index "classrooms", ["school_type_id"], name: "index_classrooms_on_school_type_id"

  create_table "classrooms_cycles", id: false, force: :cascade do |t|
    t.integer "classroom_id", null: false
    t.integer "cycle_id",     null: false
  end

  add_index "classrooms_cycles", ["classroom_id", "cycle_id"], name: "index_classrooms_cycles_on_classroom_id_and_cycle_id"

  create_table "classrooms_levels", id: false, force: :cascade do |t|
    t.integer "classroom_id", null: false
    t.integer "level_id",     null: false
  end

  add_index "classrooms_levels", ["classroom_id", "level_id"], name: "index_classrooms_levels_on_classroom_id_and_level_id"

  create_table "classrooms_users", id: false, force: :cascade do |t|
    t.integer "user_id",      null: false
    t.integer "classroom_id", null: false
  end

  add_index "classrooms_users", ["user_id", "classroom_id"], name: "index_classrooms_users_on_user_id_and_classroom_id"

  create_table "configs", force: :cascade do |t|
    t.string   "image_fond_file_name"
    t.string   "image_fond_content_type"
    t.integer  "image_fond_file_size"
    t.datetime "image_fond_updated_at"
    t.string   "bandeau_file_name"
    t.string   "bandeau_content_type"
    t.integer  "bandeau_file_size"
    t.datetime "bandeau_updated_at"
    t.boolean  "inscription_automatique", default: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "cycles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "cycle_id"
    t.integer  "test_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "documents", ["cycle_id"], name: "index_documents_on_cycle_id"
  add_index "documents", ["test_id"], name: "index_documents_on_test_id"

  create_table "instructions", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "levels", force: :cascade do |t|
    t.string   "name"
    t.integer  "order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notes", force: :cascade do |t|
    t.string   "name"
    t.float    "value"
    t.string   "picto_file_name"
    t.string   "picto_content_type"
    t.integer  "picto_file_size"
    t.datetime "picto_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "results", force: :cascade do |t|
    t.integer  "classroom_id"
    t.integer  "test_id"
    t.integer  "cycle_id"
    t.integer  "note_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "results", ["classroom_id"], name: "index_results_on_classroom_id"
  add_index "results", ["cycle_id"], name: "index_results_on_cycle_id"
  add_index "results", ["note_id"], name: "index_results_on_note_id"
  add_index "results", ["test_id"], name: "index_results_on_test_id"
  add_index "results", ["user_id"], name: "index_results_on_user_id"

  create_table "school_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tests", force: :cascade do |t|
    t.string   "name"
    t.integer  "order"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "picto_file_name"
    t.string   "picto_content_type"
    t.integer  "picto_file_size"
    t.datetime "picto_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.string   "single_access_token"
    t.string   "perishable_token"
    t.integer  "login_count",         default: 0,     null: false
    t.integer  "failed_login_count",  default: 0,     null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.boolean  "active",              default: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "admin",               default: false
    t.boolean  "pilote"
    t.boolean  "bloque"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["perishable_token"], name: "index_users_on_perishable_token", unique: true
  add_index "users", ["persistence_token"], name: "index_users_on_persistence_token", unique: true
  add_index "users", ["single_access_token"], name: "index_users_on_single_access_token", unique: true

end
