class AddSchoolTypeToUser < ActiveRecord::Migration
  def change
    add_reference :users, :school_type, index: true, foreign_key: true
  end
end
