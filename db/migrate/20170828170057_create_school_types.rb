class CreateSchoolTypes < ActiveRecord::Migration
  def change
    create_table :school_types do |t|
      t.string :name

      t.timestamps null: false
    end
    types = YAML.load_file(Rails.root.join("config", "datas.yml"))["school_types"]
    types.each do |type|
      SchoolType.create name: type
    end
  end
end
