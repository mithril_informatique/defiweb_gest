class AddAttachmentPictoToTests < ActiveRecord::Migration
  def self.up
    change_table :tests do |t|
      t.attachment :picto
    end
  end

  def self.down
    remove_attachment :tests, :picto
  end
end
