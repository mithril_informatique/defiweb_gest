class CreateLevels < ActiveRecord::Migration
  def change
    create_table :levels do |t|
      t.string :name
      t.integer :order

      t.timestamps null: false
    end
    levels = YAML.load_file(Rails.root.join("config", "datas.yml"))["levels"]
    i=0
    levels.each do |level|
      Level.create name: level, order: i
      i += 1
    end
  end
end
