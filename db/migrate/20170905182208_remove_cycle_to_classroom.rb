class RemoveCycleToClassroom < ActiveRecord::Migration
  def change
    remove_column :classrooms, :cycle_id, :integer
  end
end
