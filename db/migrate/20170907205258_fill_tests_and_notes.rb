class FillTestsAndNotes < ActiveRecord::Migration
  def change
    notes = YAML.load_file(Rails.root.join("config", "datas.yml"))["notes"]
    value = 1.0
    notes.each do |note|
      Note.create name: note, value: value
      value -= 0.5
    end

    tests = YAML.load_file(Rails.root.join("config", "datas.yml"))["tests"]
    order = 1
    tests.each do |test|
      Test.create name: test, order: order
      order += 1
    end
  end
end
