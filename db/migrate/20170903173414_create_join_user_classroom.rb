class CreateJoinUserClassroom < ActiveRecord::Migration
  def change
    create_join_table :users, :classrooms do |t|
       t.index [:user_id, :classroom_id]
    end
  end
end
