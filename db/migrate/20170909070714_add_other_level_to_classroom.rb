class AddOtherLevelToClassroom < ActiveRecord::Migration
  def change
    add_column :classrooms, :other_level, :string
  end
end
