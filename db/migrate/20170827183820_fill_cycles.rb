class FillCycles < ActiveRecord::Migration
  def change
    cycles = YAML.load_file(Rails.root.join("config", "datas.yml"))["cycles"]
    cycles.each do |cycle|
      Cycle.create name: cycle
    end
  end
end
