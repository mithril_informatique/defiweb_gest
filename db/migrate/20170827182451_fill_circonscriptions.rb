class FillCirconscriptions < ActiveRecord::Migration
  def change
    circonscriptions = YAML.load_file(Rails.root.join("config", "datas.yml"))["circonscriptions"]
    circonscriptions.each do |circonscription|
      Circonscription.create name: circonscription
    end
  end
end
