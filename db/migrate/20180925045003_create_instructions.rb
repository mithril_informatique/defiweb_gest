class CreateInstructions < ActiveRecord::Migration
  def change
    create_table :instructions do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
    Instruction.create name: "faq", description: "#FAQ\nVoici le texte de la foire aux questions"
    Instruction.create name: "inscription", description: "#Consignes d'inscription\nVoici les consignes d'inscription"
    Instruction.create name: "oubli", description: "Mot de passe oublié..."
  end
end
