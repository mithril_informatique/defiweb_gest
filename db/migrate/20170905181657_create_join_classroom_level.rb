class CreateJoinClassroomLevel < ActiveRecord::Migration
  def change
    create_join_table :classrooms, :levels do |t|
       t.index [:classroom_id, :level_id]
    end
  end
end
