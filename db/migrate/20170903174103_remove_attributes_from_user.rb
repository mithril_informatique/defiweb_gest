class RemoveAttributesFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :class_name, :string
    remove_column :users, :school_type_id, :integer
    remove_column :users, :cycle_id, :integer
    remove_column :users, :school, :string
    remove_column :users, :circonscription_id, :integer
  end
end
