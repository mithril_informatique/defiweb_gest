class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.references :classroom, index: true, foreign_key: true
      t.references :test, index: true, foreign_key: true
      t.references :cycle, index: true, foreign_key: true
      t.references :note, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
