class CreateAcademies < ActiveRecord::Migration
  def change
    create_table :academies do |t|
      t.string :name
      t.string :description

      t.timestamps null: false
    end

    liste_academies=YAML::load(File.read(Rails.root.join('config','academies.yaml')));
    liste_academies.each do |academie|
      Academie.create name: academie["name"], description: academie["description"]
    end
  end
end
