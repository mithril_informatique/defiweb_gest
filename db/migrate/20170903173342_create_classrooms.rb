class CreateClassrooms < ActiveRecord::Migration
  def change
    create_table :classrooms do |t|
      t.string :name
      t.references :circonscription, index: true, foreign_key: true
      t.references :cycle, index: true, foreign_key: true
      t.string :ecole
      t.references :school_type, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
