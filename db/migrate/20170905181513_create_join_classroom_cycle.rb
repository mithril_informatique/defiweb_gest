class CreateJoinClassroomCycle < ActiveRecord::Migration
  def change
    create_join_table :classrooms, :cycles do |t|
       t.index [:classroom_id, :cycle_id]
    end
  end
end
