class CreateConfigs < ActiveRecord::Migration
  def change
    create_table :configs do |t|
      t.attachment :image_fond
      t.attachment :bandeau
      t.boolean :inscription_automatique, default: false

      t.timestamps null: false
    end
    Config.create inscription_automatique: false
  end
end
