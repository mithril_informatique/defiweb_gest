class AddCycleToUser < ActiveRecord::Migration
  def change
    add_reference :users, :cycle, index: true, foreign_key: true
  end
end
