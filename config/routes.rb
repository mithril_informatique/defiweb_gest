Rails.application.routes.draw do

  resources :academies
  resources :instructions, only: [:index, :edit, :update]
  resources :configs, only: [:show, :edit, :update]
  get '/results/saisie', to: 'results#saisie'

  resources :results
  resources :notes
  resources :tests
  resources :levels
  resources :classrooms
  resources :school_types
  resources :cycles
  resources :circonscriptions
  resources :documents
  root 'welcome#index'
  get '/global/:id', to: 'welcome#global'
  get '/global', to: redirect('/global/0'), as: :global
  get '/users/:id/active', to: 'users#active', as: :active
  post '/users/:id/send_activation', to: 'users#send_activation', as: :send_activation

  get '/users/emails', to: 'users#emails', as: :emails_path

  get '/faq', to: 'welcome#faq', as: :faq

  get '/account', to: 'users#account', as: :account

  resources :users

  resources :user_sessions, only: [:create, :destroy]

  delete '/sign_out', to: 'user_sessions#destroy', as: :sign_out
  get '/sign_in', to: 'user_sessions#new', as: :sign_in

  resources :password_resets, only: [:new, :create, :edit, :update]

  post '/users/new_teacher', to: 'users#create_with_existing_classroom'

  get '/classroom/complet', to: 'classrooms#complet', as: :classroom_complet

  delete '/classrooms/:id/users/:user_id', to: 'classrooms#del_user', as: :classroom_del_user

  get '/tests/:id/documents', to: 'tests#documents'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
